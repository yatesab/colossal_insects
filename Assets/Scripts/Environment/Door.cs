using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{

    [SerializeField] private float openSpeed = 5f;
    [SerializeField] private float defaultPoweredOnTime = 10f;

    public bool doorPowered = false;
    public bool doorOpened = false;
    private float poweredOnTime = 0f;

    private Coroutine openDoorCoroutine;
    private Coroutine closeDoorCoroutine;

    public bool playerInRange = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckOpenDoor();
        CheckCloseDoor();
    }

    private void OnTriggerEnter(Collider other)
    {
        // If collider is triggered by energy ball
        if(other.GetComponent<EnergyBlast>() != null && !doorOpened) 
        {
            poweredOnTime = 0f;
            doorPowered = true;

            StartCoroutine("DrainDoorEnergy");
        }
    }

    public void CheckOpenDoor()
    {

        if (playerInRange && doorPowered && !doorOpened)
        {
            if(closeDoorCoroutine != null)
            {
                StopCoroutine(closeDoorCoroutine);
            }
            doorOpened = true;
            openDoorCoroutine = StartCoroutine("OpenDoor");
        }
    }

    public void CheckCloseDoor()
    {
        if (!playerInRange && !doorPowered && doorOpened)
        {
            if (openDoorCoroutine != null)
            {
                StopCoroutine(openDoorCoroutine);
            }
            doorOpened = false;
            closeDoorCoroutine = StartCoroutine("CloseDoor");
        }
    }

    IEnumerator OpenDoor()
    {
        Vector3 target = new Vector3(transform.position.x, transform.position.y + transform.localScale.y, transform.position.z);
        while(transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * openSpeed);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator CloseDoor()
    {
        Vector3 target = new Vector3(transform.position.x, transform.position.y - transform.localScale.y, transform.position.z);
        while(transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * openSpeed);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator DrainDoorEnergy()
    {
        while (poweredOnTime <= defaultPoweredOnTime)
        {
            poweredOnTime += 1;
            yield return new WaitForSeconds(1);
        }

        doorPowered = false;
    }
}
