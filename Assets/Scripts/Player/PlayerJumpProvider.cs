using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerJumpProvider : MonoBehaviour
{
  [Header("Player Jump")]
  [SerializeField] private float jumpForce = 12f;

  private Rigidbody body;
  private PlayerController playerController;

  [Header("Left Hand Jump Action")]
  [SerializeField] private InputActionReference leftJumpReference;

  [Header("Right Hand Jump Action")]
  [SerializeField] private InputActionReference rightJumpReference;

  // Start is called before the first frame update
  void Start()
  {
    body = GetComponentInParent<Rigidbody>();
    playerController = GetComponentInParent<PlayerController>();

    // Setup Left Leg Jump
    leftJumpReference.action.performed += OnJumpPress;

    // Setup Right Leg Jump
    rightJumpReference.action.performed += OnJumpPress;
  }

  // Update is called once per frame
  void Update()
  {
  }

  private void OnJumpPress(InputAction.CallbackContext obj)
  {
    Vector3 normal = playerController.GetCollisionNormal();
    if (normal != Vector3.zero)
    {
      body.AddForce(normal * jumpForce, ForceMode.VelocityChange);
    }
  }
}
