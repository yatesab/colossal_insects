using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class PlayerController : MonoBehaviour
{
  private XRRig xrRig;
  private Rigidbody body;

  [SerializeField] private int minHeight = 1;
  [SerializeField] private int maxHeight = 2;
  [SerializeField] private float fallMultiplier = 2.5f;

  private Vector3 rigCenter;
  public Vector3 GetRigCenter()
  {
    return rigCenter;
  }

  private float rigHeight;
  public float GetRigHeight()
  {
    return rigHeight;
  }

  private CapsuleCollider collisionCollider;
  private Vector3 collisionNormal;
  public Vector3 GetCollisionNormal()
  {
    return collisionNormal;
  }

  private CapsuleCollider triggerCollider;

  public bool CanMove()
  {
    return collisionNormal == Vector3.up;
  }

  // Start is called before the first frame update
  void Start()
  {
    xrRig = GetComponent<XRRig>();
    body = GetComponent<Rigidbody>();
    collisionCollider = GetComponent<CapsuleCollider>();
    triggerCollider = GetComponentInChildren<CapsuleCollider>();
  }

  void FixedUpdate()
  {
    UpdateCollider();

    UpdateGravity();
  }

  /// <summary>
  /// Update the <see cref="body.height"/> and <see cref="body.center"/>
  /// based on the camera's position.
  /// </summary>
  protected virtual void UpdateCollider()
  {
    if (xrRig == null || collisionCollider == null)
      return;

    rigHeight = Mathf.Clamp(xrRig.cameraInRigSpaceHeight, minHeight, maxHeight);

    rigCenter = xrRig.cameraInRigSpacePos;
    rigCenter.y = rigHeight / 2f + collisionCollider.radius;

    collisionCollider.height = rigHeight;
    collisionCollider.center = rigCenter;

    triggerCollider.height = rigHeight;
    triggerCollider.center = rigCenter;
  }

  private void UpdateGravity()
  {
    if (body.velocity.y <= 0)
    {
      body.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
    }
  }

  void OnCollisionEnter(Collision collision)
  {
    if (collision.gameObject.tag == "Ground")
    {
      collisionNormal = CheckCollisionNormal(collision);
    }

    if (collision.gameObject.tag == "Climbable")
    {
      collisionNormal = CheckCollisionNormal(collision);
    }
  }

  void OnCollisionExit(Collision collision)
  {
    if (collision.gameObject.tag == "Ground")
    {
      collisionNormal = Vector3.zero;
    }
  }

  void OnTriggerExit(Collider collider)
  {
    if (collider.gameObject.tag == "Climbable")
    {
      collisionNormal = Vector3.zero;
    }
  }

  private Vector3 CheckCollisionNormal(Collision collision)
  {
    if (collision.contacts.Length > 0)
    {
      return collision.contacts[0].normal;
    }

    return Vector3.zero;
  }
}
