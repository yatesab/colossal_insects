using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRigTracker : MonoBehaviour
{
    [SerializeField] private float followSpeed = 50f;
    [SerializeField] private float rotationSpeed = 100f;

    [Space]

    [SerializeField] private Vector3 positionOffset;
    [SerializeField] private Vector3 rotationOffset;

    private GameObject playerRig;
    private CapsuleCollider collider;
    private Rigidbody leftHand;
    private Rigidbody rightHand;

    private Transform playerTransform;
    private Transform cameraTransform;
    private Transform leftHandTransform;
    private Transform rightHandTransform;
    private Transform waistTransform;

    // Start is called before the first frame update
    void Start()
    {
        // Rig Objects
        playerRig = GameObject.Find("/Player Rig");
        collider = playerRig.GetComponent<CapsuleCollider>();

        playerTransform = playerRig.transform;
        cameraTransform = playerTransform.Find("Camera Offset/Main Camera");
        leftHandTransform = playerTransform.Find("Camera Offset/Controller Left");
        rightHandTransform = playerTransform.Find("Camera Offset/Controller Right");

        // Model Objects
        leftHand = GameObject.Find("/Player Model/Left Hand").GetComponentInChildren<Rigidbody>();
        rightHand = GameObject.Find("/Player Model/Right Hand").GetComponentInChildren<Rigidbody>();

        waistTransform = transform.Find("Player Waist");
    }

    // Update is called once per frame
    void Update()
    {
        // Follow Rigs Position
        transform.position = playerTransform.position;

        // Follow Rigs Rotation
        transform.rotation = playerTransform.rotation;

        //Waist Tracking
        Vector3 newLocalPosition = collider.center;

        newLocalPosition.y = collider.height / 2.5f;

        // Set new position in local space
        waistTransform.localPosition = newLocalPosition;

        Vector3 eulerRotation = new Vector3(transform.eulerAngles.x, cameraTransform.eulerAngles.y, transform.eulerAngles.z);

        waistTransform.rotation = Quaternion.Euler(eulerRotation);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (leftHand != null)
        {
            PhysicsMoveHands(leftHandTransform, leftHand);
        }
        if (rightHand != null)
        {
            PhysicsMoveHands(rightHandTransform, rightHand);
        }
    }

    private void PhysicsMoveHands(Transform followTarget, Rigidbody body)
    {
        //Position
        var positionWithOffset = followTarget.TransformPoint(positionOffset);
        var distance = Vector3.Distance(positionWithOffset, body.transform.position);
        body.velocity = (positionWithOffset - body.transform.position).normalized * (followSpeed * distance);

        //Rotation
        var rotationWithOffset = followTarget.rotation * Quaternion.Euler(rotationOffset);
        var q = rotationWithOffset * Quaternion.Inverse(body.rotation);
        q.ToAngleAxis(out float angle, out Vector3 axis);
        body.angularVelocity = axis * (angle * Mathf.Deg2Rad * rotationSpeed);
    }
}
