using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CannonBoost : MonoBehaviour
{
  private CannonController cannon;

  private CannonGrapple cannonGrapple;
  public float _coolDown = 0f;
  private Coroutine _boostCoroutine;

  // Audio Props
  private AudioSource _boostAudioSource;
  private string _boostAudioName = "Air_Boost";

  [Header("Air Boost Values")]
  [SerializeField] private float boostAmount = 30f;
  [SerializeField] private float coolDownTime = 5f;

  [Header("Line Boost Values")]
  [SerializeField] private float lineSpring = 20f;
  [SerializeField] private float lineDamper = 10f;

  // Start is called before the first frame update
  void Start()
  {
    cannonGrapple = GetComponent<CannonGrapple>();
    cannon = GetComponent<CannonController>();

    // Pull Audio Sources for playback
    //AudioSource[] audioSources = GetComponents<AudioSource>();
    //foreach (AudioSource audio in audioSources)
    //{
    //    if (audio.clip.name == _boostAudioName)
    //    {
    //        _boostAudioSource = audio;
    //    }
    //}
  }

  void Update()
  {
    if (_coolDown > 0f)
    {
      _coolDown -= Time.deltaTime;
    }
  }

  private void AirBoost()
  {
    //if (_boostAudioSource != null)
    //{
    //    _boostAudioSource.Play();
    //}

    cannon.GetPlayerBody().AddForce(-transform.forward * boostAmount, ForceMode.VelocityChange);
  }

  public void StartBoost()
  {
    if (_coolDown > 0) return;
    _coolDown = coolDownTime;

    //if (_boostAudioSource != null)
    //{
    //    _boostAudioSource.Play();
    //}

    if (cannon.IsGrappling())
    {
      _boostCoroutine = StartCoroutine(LineBoost());
    }
    else
    {
      AirBoost();
    }
  }

  public void EndBoost()
  {
    if (_boostCoroutine != null)
    {
      StopCoroutine(_boostCoroutine);
      cannon.GetPlayerBody().useGravity = true;
    }
  }

  private IEnumerator LineBoost()
  {
    Rigidbody playerBody = cannon.GetPlayerBody();
    SpringJoint joint = cannon.GetJoint();

    //playerBody.AddForce(-playerBody.velocity, ForceMode.VelocityChange);
    playerBody.useGravity = false;
    joint.spring = lineSpring;
    joint.damper = lineDamper;

    // Start Lowering Distance
    while (joint != null && cannonGrapple.DistanceToGrapplePoint() > 0f)
    {
      if (joint.maxDistance > 0f)
      {
        joint.maxDistance -= 10f;
      }
      else
      {
        Vector3 newForce = (cannonGrapple.GetEndPoint() - cannonGrapple.GetAttachPoint().position) * Time.deltaTime;
        playerBody.AddForce(newForce, ForceMode.VelocityChange);
      }

      yield return new WaitForEndOfFrame();
    }

    playerBody.useGravity = true;
  }
}
