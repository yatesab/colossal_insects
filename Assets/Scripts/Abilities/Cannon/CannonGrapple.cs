using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class CannonGrapple : MonoBehaviour
{
    private Transform _playerPosition, _attachPoint, _grapplePosition;
    private Rigidbody _playerBody;
    private XRRayInteractor _rayInteractor;
    private LineRenderer lineRenderer;
    private Vector3 _endPoint, _grappleLocalOffset;
    private RaycastHit _controllerRaycastHit;

    //Coroutines
    private Coroutine _grappleCoroutine;

    // Audio Props
    private AudioSource _threadAudioSource, _retractAudioSource;
    private string _threadAudioName = "Thread_Shot", _retractAudioName = "Retract_Thread";

    [Header("Line Values")]
    [SerializeField] private Material lineMaterial;
    [SerializeField] private float lineWidth = 0.01f;

    [Header("Joint Values")]
    [SerializeField] private float grappleSpeed = 2.5f;
    [SerializeField] private float startingTension = 0.9f;
    [SerializeField] public float startingSpring = 10f;
    [SerializeField] public float startingDamper = 5f;

    [Header("Retract Values")]
    [SerializeField] private float retractSpeed = 0.3f;

    private CannonController cannon;

    // Start is called before the first frame update
    void Start()
    {
        // Get Objects
        cannon = GetComponent<CannonController>();

        _playerPosition = GameObject.Find("/Player Rig").transform;
        _attachPoint = GameObject.Find("Player Model/" + cannon.GetHandSide() + " Hand/Arm Cannon/Attach Point").transform;
        _rayInteractor = GetComponentInParent<XRRayInteractor>();

        // // Get Audio Sources for playback
        // AudioSource[] audioSources = GetComponents<AudioSource>();
        // foreach (AudioSource audio in audioSources)
        // {
        //     if (audio.clip.name == _threadAudioName)
        //     {
        //         _threadAudioSource = audio;
        //     }
        //     if (audio.clip.name == _retractAudioName)
        //     {
        //         _retractAudioSource = audio;
        //     }
        // }
    }

    void LateUpdate()
    {
        // Check for connection and update connectedAnchor incase it moved
        if (cannon.GetJoint() != null)
        {
            _endPoint = _grapplePosition.TransformPoint(_grappleLocalOffset);
            cannon.GetJoint().connectedAnchor = _endPoint;
        }

        //Draw the grapple for this frame
        if (lineRenderer != null)
        {
            DrawGrapple();
        }
    }

    /********************
    * Utility Functions *
    *********************/
    private void DrawGrapple()
    {
        // Set the start point at the controller position
        lineRenderer.SetPosition(0, _attachPoint.position);

        lineRenderer.SetPosition(1, _endPoint);
    }


    private void SetupJoint()
    {
        _grapplePosition = _controllerRaycastHit.transform;
        _endPoint = _controllerRaycastHit.point;

        //Create the joint
        SpringJoint joint = _playerPosition.gameObject.AddComponent<SpringJoint>();

        // Set up player anchor and object anchor
        joint.autoConfigureConnectedAnchor = false;
        joint.enableCollision = true;
        joint.anchor = _playerPosition.InverseTransformPoint(_attachPoint.position);

        _grappleLocalOffset = _grapplePosition.InverseTransformPoint(_endPoint);

        joint.connectedAnchor = _grapplePosition.TransformPoint(_grappleLocalOffset);

        // Create initial maxDistance for joint and other spring settings
        float distanceFromPoint = Vector3.Distance(_attachPoint.position, _endPoint);
        joint.maxDistance = distanceFromPoint * startingTension;
        joint.massScale = cannon.GetPlayerBody().mass;
        joint.spring = startingSpring;
        joint.damper = startingDamper;

        cannon.SetJoint(joint);
    }

    public void LowerJointMaxDistance(float distanceSubtract)
    {
        SpringJoint joint = cannon.GetJoint();

        if (joint == null || joint.maxDistance < 0f) return;
        joint.maxDistance -= distanceSubtract;

        // if (_retractAudioSource.isPlaying) return;
        // _retractAudioSource.Play();
    }

    /******************
     * Button Actions *
     ******************/
    public void StartGrapple()
    {
        if (cannon.GetJoint() != null) return;

        if (_rayInteractor.TryGetCurrent3DRaycastHit(out _controllerRaycastHit))
        {
            cannon.SetIsGrappling(true);

            // Add Line Renderer
            lineRenderer = _attachPoint.gameObject.AddComponent<LineRenderer>();

            // Configure Line Renderer
            lineRenderer.startWidth = lineWidth;
            lineRenderer.endWidth = lineWidth;
            lineRenderer.material = lineMaterial;

            _grappleCoroutine = StartCoroutine(FireGrapple());
        }
    }

    public void StopGrapple()
    {
        if (_grappleCoroutine != null)
        {
            StopCoroutine(_grappleCoroutine);
        }

        if (cannon.GetJoint() != null) Destroy(cannon.GetJoint());
        if (lineRenderer != null) Destroy(lineRenderer);

        cannon.SetIsRetracting(false);
        cannon.SetIsGrappling(false);

        // if (_retractAudioSource.isPlaying)
        // {
        //     _retractAudioSource.Stop();
        // }
    }

    public void StartRetract()
    {
        if (cannon.GetJoint() != null)
        {
            cannon.SetIsRetracting(true);

            StartCoroutine(RetractPlayer());
        }
    }

    public void StopRetract()
    {
        if (cannon.GetJoint() != null)
        {
            cannon.SetIsRetracting(false);
            // if (_retractAudioSource.isPlaying)
            // {
            //     _retractAudioSource.Stop();
            // }
        }
    }

    /**************
     * Coroutines *
     **************/
    private IEnumerator FireGrapple()
    {
        //Play Grapple Shooting Sound
        //if (_threadAudioSource != null)
        //{
        //    _threadAudioSource.Play();
        //}

        // Get starting point
        _endPoint = _attachPoint.position;

        // Start Moving _endpoint towards the raycastHit
        while (Vector3.Distance(_endPoint, _controllerRaycastHit.point) > 0f )
        {
            _endPoint = Vector3.MoveTowards(_endPoint, _controllerRaycastHit.point, grappleSpeed);
            yield return new WaitForEndOfFrame();
        }

        SetupJoint();
    }

    private IEnumerator RetractGrapple()
    {
        if (cannon.GetJoint() != null) Destroy(cannon.GetJoint());

        // Start Moving _endpoint towards the raycastHit
        while (Vector3.Distance(_endPoint, _attachPoint.position) > 1f)
        {
            _endPoint = Vector3.MoveTowards(_endPoint, _attachPoint.position, grappleSpeed * cannon.GetPlayerBody().velocity.magnitude);
            yield return new WaitForEndOfFrame();
        }

        if (lineRenderer != null) Destroy(lineRenderer);
    }

    private IEnumerator RetractPlayer()
    {
        while (cannon.GetTriggerButton() == 1)
        {
            LowerJointMaxDistance(retractSpeed);
            yield return new WaitForEndOfFrame();
        }
    }

    /*****************
     * Get Functions *
     *****************/
    public float DistanceToGrapplePoint()
    {
        return Vector3.Distance(_attachPoint.position, _endPoint);
    }

    public Transform GetAttachPoint()
    {
        return _attachPoint;
    }

    public Vector3 GetEndPoint()
    {
        return _endPoint;
    }
}
