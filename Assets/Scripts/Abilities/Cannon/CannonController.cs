using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CannonController : MonoBehaviour
{
  private CannonGrapple cannonGrapple;
  private CannonBoost cannonBoost;
  private CannonGun cannonGun;

  private Rigidbody playerBody;
  public Rigidbody GetPlayerBody()
  {
    return playerBody;
  }

  private SpringJoint joint;
  public SpringJoint GetJoint()
  {
    return joint;
  }
  public void SetJoint(SpringJoint newJoint)
  {
    joint = newJoint;
  }

  private bool isGrappling, isRetracting;
  public bool IsGrappling()
  {
    return isGrappling;
  }
  public void SetIsGrappling(bool state)
  {
    isGrappling = state;
  }
  public bool IsRetracting()
  {
    return isRetracting;
  }
  public void SetIsRetracting(bool state)
  {
    isRetracting = state;
  }

  public float GetTriggerButton()
  {
    return retractProperty.action.ReadValue<float>();
  }

  public enum hands
  {
    Left,
    Right
  };
  [Header("Hand Side")]
  [SerializeField] private hands handside;
  public hands GetHandSide()
  {
    return handside;
  }

  [Header("Fire Controls")]
  [SerializeField] private InputActionProperty shootCannonProperty;

  [Header("Grapple Controls")]
  [SerializeField] private InputActionProperty grappleProperty;
  [SerializeField] private InputActionProperty retractProperty;

  [Header("Boost Controls")]
  [SerializeField] private InputActionProperty boostProperty;

  // Start is called before the first frame update
  void Start()
  {
    playerBody = GetComponentInParent<Rigidbody>();
    cannonGrapple = GetComponent<CannonGrapple>();
    cannonBoost = GetComponent<CannonBoost>();
    cannonGun = GetComponent<CannonGun>();

    // Get Thread Action
    grappleProperty.action.performed += OnGripPress;
    grappleProperty.action.canceled += OnGripRelease;

    // Get Retract Action
    retractProperty.action.performed += OnTriggerPress;
    retractProperty.action.canceled += OnTriggerRelease;

    // Get Shoot Action
    shootCannonProperty.action.performed += OnTriggerPress;
    shootCannonProperty.action.canceled += OnTriggerRelease;

    // Get Boost Action
    boostProperty.action.performed += OnSecondaryPress;
    boostProperty.action.canceled += OnSecondaryRelease;
  }

  // Update is called once per frame
  void Update()
  {

  }

  /******************
   * Button Actions *
   ******************/
  private void OnTriggerPress(InputAction.CallbackContext obj)
  {
    if (isGrappling)
    {
      if (cannonGrapple != null)
      {
        cannonGrapple.StartRetract();
      }
    }
    else
    {
      if (cannonGun != null)
      {
        cannonGun.ChargeGun();
      }
    }
  }

  private void OnTriggerRelease(InputAction.CallbackContext obj)
  {
    if (isGrappling)
    {
      if (cannonGrapple != null)
      {
        cannonGrapple.StopRetract();
      }
    }
    else
    {
      if (cannonGun != null)
      {
        cannonGun.FireGun();
      }
    }
  }

  private void OnGripPress(InputAction.CallbackContext obj)
  {
    if (cannonGrapple != null)
    {
      cannonGrapple.StartGrapple();
    }
  }

  private void OnGripRelease(InputAction.CallbackContext obj)
  {
    if (cannonGrapple != null)
    {
      cannonGrapple.StopGrapple();
    }
  }

  private void OnSecondaryPress(InputAction.CallbackContext obj)
  {
    if (cannonBoost != null)
    {
      cannonBoost.StartBoost();
    }
  }

  private void OnSecondaryRelease(InputAction.CallbackContext obj)
  {
    if (cannonBoost != null)
    {
      cannonBoost.EndBoost();
    }
  }
}
