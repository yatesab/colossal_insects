using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class CannonGun : MonoBehaviour
{
    public float projectileSpeed = 20f;
    public float chargeSpeed = 6f;

    public GameObject EnergyPrefab;

    private float pressTime;
    private Transform _attachPoint;

    private CannonController cannon;

    // Start is called before the first frame update
    void Start()
    {
        cannon = GetComponent<CannonController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FireProjectile(float holdTime)
    {
        GameObject energyShot = Instantiate(EnergyPrefab, transform.TransformPoint(new Vector3(0f, 0f, 0.2f)), Quaternion.identity);
        Vector3 newScale = energyShot.transform.localScale * (holdTime * chargeSpeed);

        newScale.x = Mathf.Clamp(newScale.x, 0.1f, 2f);
        newScale.z = Mathf.Clamp(newScale.z, 0.1f, 2f);
        newScale.y = Mathf.Clamp(newScale.y, 0.1f, 2f); ;

        energyShot.transform.localScale = newScale;

        EnergyBlast blast = energyShot.GetComponent<EnergyBlast>();
        Rigidbody body = energyShot.GetComponent<Rigidbody>();

        blast.Setup(transform.position);

        // Move projectile towards its location
        body.AddForce(transform.TransformDirection(Vector3.forward) * projectileSpeed, ForceMode.VelocityChange);
    }

    public void ChargeGun()
    {
        pressTime = Time.time;
    }

    public void FireGun()
    {
        FireProjectile(Time.time - pressTime);
    }
}
