using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBlast : MonoBehaviour
{
    public float damage = 10f;
    public float distance = 100f;

    private Vector3 _firePoint;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(transform.position, _firePoint) > distance){
            Destroy(gameObject);
        }
    }

    public void Setup(Vector3 firePoint){
        _firePoint = firePoint;
    }

    void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
